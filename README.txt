Opus filter for Drupal
======================

Description:
------------
        
This module enables you to enter visual formating tags to node body.
The tags are taken from Opus documentor http://www.opus.ch.
Available tags are listed on filter tips page.

Note:
-----

Filter should be lighter than URL_filter (e.g. tilde in http://www.domain.tld/~home forces formating)

Todo:
-----
improve parsing of list, columns - themes
add styles
UTF-8/multi-byte ???
regular expressions ereg_ -> preg_

Authors
-------
Tomas Mandys (tomas.mandys@2p.cz)

